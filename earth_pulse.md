# The Sense of the Planet

### Experience the concerns of the planet. <br> 									Around the main challenges it’s facing. <br> 									It’s all in your hands.

## Concept & Purpose 
The "Sense of the Planet" started as a project that would fuse advanced design techniques in digital fabrication, electronics and a strong message through data visualization.  The last part was a critical one, since we were looking to create something that would cause a level of influence through immersing ourselves into the experience. The purpose was to create an experience that allowed to measure the concerns of the world through visual sensing. 

## Brainstorming & Planning 

<div align="center"> 
<img src="Files/Artifact/ides.jpg">
<div align="left">

With some initial inspiration in mind we started planning how we could execute this project in an efficient and interconnected way. To master our project we split up in three parts and worked simultaneously across them: fabrication, electronics and documentation. 

## Fabrication

#### Step I
Sketching: We started sketching different types of spheres amongst others. 

<div align="center"> 
<img src="Files/Fabrication/PICTURE_01.png">


<div align="left"> 

#### Step II
3D Modeling + 3D Printing: We generated the iterations by drawing the models in rhinoceros and had to be directly related to how the pieces were going to be printed (that’s the reason we had to made half sphere and not a single complete sphere). 

<div align="center"> 
<img src="https://media.giphy.com/media/l5LSVl1QE4TSpr8KhK/giphy.gif">

 <img src="Files/Fabrication/PICTURE_02.png">

<img src="Files/Fabrication/PICTURE_03.png">

<div align="left"> 

#### Step III

Functionality: In parallel to the geometric iterations, we began to evaluate what type of interaction we wanted to achieve with this prototype; we wanted it to vibrate or let the light pass through. For this reason, the geometric figure and the chosen material used had to be in accordance with the function we wanted to achieve. At the same time, we were sketching the sphere protype, we questioned ourselves how we wanted others to interact with it. For this we thought it would be practical to use cnc and make a laser engraved text to make a small stand to place the sphere and have a brief explanation of it. 

#### Step IV

Fabrication Processes and Materials: The materials were directly related to the function, which in this case was to choose a material that allowed light to go through it (once defined to use LEDs in electronics instead of vibration). We made each sphere separately; on the one hand we did 3D printed the geodesic half sphere with resin (machine: FormLabs +1). We printed prototypes with a dimension of 6cm. The final prototye was a sphere of 10cm diameter. Once again, We made each sphere separately; on the one hand we did 3D printed the geodesic half sphere with resin (machine: FormLabs +1). To showcase the concept we assemble a simple stand fabricated in cnc (9mm wood). 

<div align="center">
<center><img src="https://media.giphy.com/media/iqAJ0lsKjLRHzsuysq/giphy.gif"></center>

<div align="center"> 
<img src="Files/Fabrication/PICTURE_04.jpeg">
<div align="left">

<br>

## Electronics
Electronic elements: 

- Arduino Board  
- LEDs 
- Single Cell LiPo Battery
- Esp32  Feather Board 

![](Files/Electronics/pres1.png)

#### IFTT
![](Files/Electronics/pres2.png)

[Video](https://vimeo.com/526126169)


#### BLYNK
![](Files/Electronics/pres3.png)

#### ESP
![](Files/Electronics/pres4.png)



## Final Prototype 
After working simultaneously across fabrication & electronics, we combined both workstreams and prototype the final artifact. The artifact lights up green whenever someone in the world tweets #savetheplanet, and it turns red when someone tweets #climatestrike.
 
<div align="center"> 
<img src="Files/Artifact/PICTURE5.jpeg">

<img src="Files/Artifact/PICTURE6.jpeg">	

<div align="left">

<iframe width="560" height="315" src="https://www.youtube.com/embed/pStRKAMEbKM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Video](https://www.youtube.com/watch?v=pStRKAMEbKM)

## Learnings & Iterations

#### Fabrication
- 3D printing: While the elements printed in resin worked accurately from the beginning, we faced some issues when printing the half sphere made in pla. The main issue of printing a half sphere was adding the supporting point correctly. After various iterations we put an additional layer on the bottom of the half sphere to give it stability. This was neccesary to make the printing process happen. 
- Attaching elements: Another issue that we faced was how to attach each half sphere. We designed a way to embed them but it failed so we ended up by sticking them.  

<div align="center"> 
<img src="Files/Fabrication/PICTURE_07.jpeg">
<div align="left">

#### Electronics 
- 3rd Party Provider: When working with a 3rd party provider, the process of taking over their exemplary coding logics is helpful and makes the process more efficient. Nevertheless, it is not always clear what sits behind the coding logic, which makes it challenging to understand the code - especially as an amateur. Also, there is a need to do a lot of research to get all the information (URL, Auth Token) right in the end. Previous experience reviews from other individuals that were publicly available helped a lot. But ITFFF is a strong programm for data visualisation. 
- Limitations: IFTT was not always properly updating the applets and notifications were therefore not received correctly. Another limitation we faced was the connection to Wi-fi networks, as we oftentimes failed connecting to them due to blockers or hotspots that were not stable enough. 


## Future Development Opportunities 
- Create different experiences with a variety of senses (e.g. vibration)
- Allow for mutual interaction & interconnectivity with device
- Data visualization as part of an exhibition 

## Links to Websites 
[Jose Uribe](https://jose_uribe.gitlab.io/mdef/index.html)
[Josefina Nano](https://josefina_nano.gitlab.io/mdef-website/index.html)
[David Wyss](https://david_wyss.gitlab.io/mdef-class/30.1_FabChallenge2.html)

